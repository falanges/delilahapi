require("dotenv").config();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const volatileUser = require("../middlewares/users");
const userSchema = require("../models/db.userSchema");

//Encrypt password
const crypty = async (data) => {
  let pass = await bcrypt.hash(data, 8);
  return pass;
};

//Decrypt password
const decripty = async (data, old) => {
  let aux = await bcrypt.compare(data, old);

  return aux;
};

const createToken = async (data) => {
  let dataToken = { _id: "", role: data.role };
  data._id = data._id.toString();
  dataToken._id = data._id;
  const token = jwt.sign(dataToken, process.env.JWT_SECRET, {
    expiresIn: "24h",
  });
  return token;
};

let get = () => {
  return (exists = volatileUser.activeUsers); // Take all existing users
};

let userFinder = async (user) => {
  //console.log(user);
  return (exists = await userSchema.findOne({ _id: user.id }));
  // If it doesn't exists, it throws "-1"
};

let add = async (newUser) => {
  // If user and mail exists before create them
  // then throw a value of "-1" (Case of user inexistence)
  // and if the user doesn't exists then find mail in order to not send repeated message
  let exists = await userSchema.findOne({ user: newUser.user });
  let mailExists = await userSchema.findOne({ mail: newUser.mail });
  if (exists === null && mailExists === null) {
    newUser.password = await crypty(newUser.password);
    let peterUser = userSchema(newUser);
    peterUser.save();
    return (msg = { text: `User setup ok` });
  } else {
    return (msg = { text: `The user or email already exists` });
  }
};

let login = async (user, password) => {
  let exists = userFinder(user);
  let aux;
  let msg;
  let something = await userSchema.findOne({ user: user }).then((res) => {
    aux = res;
  });
  if (exists >= 0 || aux != null) {
    let cualca = await decripty(password, aux.password);
    if (cualca) {
      let token = await createToken(aux);
      volatileUser.activeUsers[0] = token;
      return (msg = {
        text: `User well logged`,
        token: token,
      });
    } else {
      return (msg = { text: `Wrong user or password!` });
    }
  } else {
    return (msg = { text: `Wrong user or password!` });
  }
};

let update = async (userUpdate, res) => {
  let exists = await userFinder(userUpdate);
  //console.log(userUpdate);
  if (exists === null) {
    return `User not reached or not created`;
  } else {
    if (exists.mail !== userUpdate.mail) {
      let verifyMail = userUpdate.mail;
      if (verifyMail >= 0) {
        return `The user already have the same mail address`;
      }
    }
    // let modify only if it's not a blank field'
    if (userUpdate.user !== undefined) {
      exists.user = userUpdate.user;
    }
    if (userUpdate.fullname !== undefined) {
      exists.fullname = userUpdate.fullname;
    }
    if (userUpdate.mail !== undefined) {
      exists.mail = userUpdate.mail;
    }
    if (userUpdate.phone !== undefined) {
      exists.phone = userUpdate.phone;
    }
    if (userUpdate.address !== undefined) {
      exists.address.push(userUpdate.address);
    }
    if (userUpdate.password !== undefined) {
      exists.password = await crypty(userUpdate.password);
    }
    if (userUpdate.role !== undefined) {
      exists.role = userUpdate.role;
    }
    login(exists.user, exists.password);
    userSchema
      .updateOne(
        { _id: exists.id },
        {
          $set: {
            user: exists.user,
            fullname: exists.fullname,
            mail: exists.mail,
            phone: exists.phone,
            address: exists.address,
            password: exists.password,
            role: exists.role,
          },
        }
      )
      .then((data) => res.json("User modified with style"))
      .catch((error) => res.json({ message: "Something has been wrong" }));
  }
};

let delet = (username) => {
  let index = userFinder(username);
  if (index === -1) {
    return `User not found`;
  } else {
    volatileUser.users.splice(index, 1);
    return `User erradicated`;
  }
};

let getView = () => {
  return volatileUser.activeUsers.length === 0
    ? `Nobody logged`
    : volatileUser.activeUsers;
};

let logout = () => {
  volatileUser.activeUsers = [];
  return `Session exited successfully`;
};
module.exports = {
  get,
  add,
  login,
  delet,
  update,
  logout,
  getView,
  userFinder,
};
