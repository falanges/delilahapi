FROM node:lts-slim
ENV NODE_ENV=production
RUN apt update && apt install tzdata -y
RUN cp /usr/share/zoneinfo/America/Argentina/Ushuaia /etc/localtime
RUN echo "America/Argentina/Ushuaia" >  /etc/timezone
RUN mkdir /delilahapi
WORKDIR /delilahapi
COPY package*.json ./
RUN npm i -g all-the-package-names
RUN npm i -g nodemon
# COPY . .
EXPOSE 4000
CMD ["npm", "run", "dev"]