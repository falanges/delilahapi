require('dotenv').config();
const { AssertionError } = require('assert');
const assert = require('assert');
const fetch = require('cross-fetch');

describe(`TEST APIs`, () => {
    it(`POST/users/login`, async () => {
        const url = `http://localhost:4001/users/login`;
        const data = {
            user: 'test',
            password: 'test'
        };
        await fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                assert.equal(response.status, 200);
            });
    });

    it(`POST/users/logout`, async () => {
        const url = `http://localhost:4001/users/logout`;
        const data = {
            token: 'test'
        };
        await fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                assert.equal(response.status, 200);
            });
    });

});