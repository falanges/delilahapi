const express = require("express");
const router = express.Router();
router.use(express.urlencoded({ extended: true }));
const orders = require("../functions/orders.js");
const midUsers = require("../middlewares/users");
const orderSchema = require("../models/db.orderSchema");
const orderStatusSchema = require("../models/db.orderStatusSchema");

/**
 * @swagger
 * tags:
 *   name: Orders
 *   description: CRUD orders operations.
 * paths:
 *   /orders/all_of/:
 *    get:
 *      description: Lists all existent orders
 *      summary: "Admins can list all the orders"
 *      tags: [Orders]
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/all_of/", async (req, res) => {
  try {
    let allOrders = await orderSchema.find();
    res.status(200).json(allOrders);
  } catch (error) {
    console.error(error);
    res.status(400).json({
      msg: "Crazy error",
    });
  }
});

/**
 * @swagger
 * tags:
 *   name: Orders
 *   description: CRUD orders operations.
 * paths:
 *   /orders/one_of/{id}:
 *    get:
 *      description: Lists one order
 *      summary: "Admins can look at one order"
 *      parameters:
 *      - name: id
 *        description: Id order to get data for
 *        in: path
 *        required: true
 *        type: string
 *      tags: [Orders]
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/one_of/:id", async (req, res, next) => {
  let oneOrder = await orderSchema.findOne();
  res.json(oneOrder);
});

/**
 * @swagger
 * paths:
 *   /orders/all_user_orders/{id}/:
 *    get:
 *      description: Lists all user orders
 *      summary: "Lists all user orders"
 *      parameters:
 *      - name: id
 *        description: Id user to get data for
 *        in: path
 *        required: true
 *        type: string
 *      tags: [Orders]
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/all_user_orders/:id", midUsers.adminUser, async (req, res) => {
  try {
    let userOrders = await orderSchema.find().populate("_id").exec();
    res.json(userOrders);
  } catch (err) {
    console.log(err);
    res.send(err);
  }
});

// /**
//  * @swagger
//  * paths:
//  *   /{id}:
//  *    get:
//  *      description: Shows order details
//  *      summary: "Shows order details"
//  *      parameters:
//  *      - name: id
//  *        description: id order to search for
//  *        summary: "Returns order details"
//  *        in: path
//  *        required: true
//  *        type: integer
//  *      tags: [Orders]
//  *      responses:
//  *        200:
//  *          description: Success
//  */
// router.get("/:id", function(req, res) {
//     let id = parseInt(req.params.id, 10);
//     res.json(orders.getId(id));
// });

/**
 * @swagger
 * paths:
 *   /orders:
 *    post:
 *      tags: [Orders]
 *      description: Creates new order, the user gets the loggin dataset
 *      summary: "Creates new order"
 *      parameters:
 *      - name: address
 *        description: Deliver Address. If not specified, it takes de address of user account
 *        in: formData
 *        required: true
 *        type: string
 *        enum: [user address, another address]
 *      - name: payment_opt
 *        description: Must select some payment option
 *        in: formData
 *        required: true
 *        type: string
 *        enum: [ "Credit Card", "Debit Card", "Cash"]
 *        style: form
 *      - name: products
 *        description: Must select some items
 *        in: formData
 *        required: true
 *        type: string
 *        enum: [ "Focaccia", "Classic Burger", "Veggie Sandwich", "Salmon Bagel", "Veggie Salad", "Focaccia Sandwich"]
 *        style: form
 *      - name: to_pay
 *        description: Shows total amount to pay for this order
 *        in: formData
 *        required: false
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/", async (req, res) => {
  try {
    const order = await orders.addOrder(req.body);
    res.json(order);
  } catch (err) {
    res.status(400).json({
      msg: "Error tramboliko",
    });
  }
});

/**
 * @swagger
 * paths:
 *   /orders/status:
 *    get:
 *      description: Lists all user order status
 *      summary: "Lists all status available"
 *      tags: [Status]
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/status", async (req, res) => {
  let orderStatus = await orderStatusSchema.find();
  res.json(orderStatus);
});

/**
 * @swagger
 * paths:
 *   /orders/status:
 *    post:
 *      description: Creates new status, only used to load static content
 *      summary: "Creates new status"
 *      tags: [Orders]
 *      parameters:
 *      - name: status
 *        description: Enter a brand new available status for all orders.
 *        in: formData
 *        required: false
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/status", async (req, res, next) => {
  const status = await orders.addNewStatus(req.body, "addStatus");
  res.json(status);
});

/**
 * @swagger
 * paths:
 *   /orders/{id}:
 *    put:
 *      tags: [Orders]
 *      description: Updates the order
 *      summary: "Updates the order"
 *      parameters:
 *      - name: id
 *        description: Order id to update
 *        in: path
 *        required: true
 *        type: integer
 *      - name: delivery_address
 *        description: Order delivery address. If not filled, it takes de address of user account
 *        in: formData
 *        required: false
 *        type: string
 *      - name: payment_opt
 *        description: Select payment option
 *        in: formData
 *        required: false
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/:id", function (req, res) {
  let id = parseInt(req.params.id, 10);
  res.json({
    msj: orders.update(req.body, id),
  });
});

/**
 * @swagger
 * paths:
 *   /orders/{id}/{statusId}:
 *    put:
 *      tags: [Orders]
 *      description: Update status of an order
 *      summary: "Admins can update status of an order"
 *      parameters:
 *      - name: id
 *        description: Id order to let the status change
 *        in: path
 *        required: true
 *        type: integer
 *      - name: statusId
 *        description: New order status
 *        in: path
 *        required: false
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/:id/:statusId", midUsers.adminUser, function (req, res, next) {
  let id = parseInt(req.params.id, 10);
  let statusId = parseInt(req.params.statusId, 10);
  res.json({ msj: orders.statusUpdate(id, statusId) });
});

/**
 * @swagger
 * paths:
 *   /status/{id}:
 *    put:
 *      tags: [status]
 *      description: Updates the selected status
 *      summary: "Updates the selected status"
 *      parameters:
 *      - name: id
 *        description: Status id to update
 *        in: path
 *        required: true
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/status/:id", (req, res, next) => {
  const { status } = req.params;
  //    const { status } = req.body;
  //.update({status: status},)
});

/**
 * @swagger
 * paths:
 *   /orders/{orderId}/products/{productId}:
 *    post:
 *      tags: [Orders]
 *      description: Add an item on the order
 *      summary: "Add an item on the order"
 *      parameters:
 *      - name: orderId
 *        description: Order id
 *        in: path
 *        required: true
 *        type: integer
 *      - name: productId
 *        description: Product id
 *        in: path
 *        required: true
 *        type: integer
 *      - name: quantity
 *        description: Total amount of each item in the order
 *        in: formData
 *        required: true
 *        type: integer
 *      - name: notes
 *        description: Some additional annotations
 *        in: formData
 *        required: false
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/:orderId/products/:productId", async (req, res) => {
  console.log("Ruta Hacia productos");
  let orderId = parseInt(req.params.orderId, 10);
  let productId = parseInt(req.params.productId, 10);
  const order = await orders.addProdToOrder(req.body, "addProduct");
  res.json({
    msj: orders.addProduct(req.body, orderId, productId),
  });
});

/**
 * @swagger
 * paths:
 *   /orders/{orderId}/products/{productId}:
 *    put:
 *      tags: [Orders]
 *      description: Updates an order's new product
 *      summary: "Updates a given product on the order"
 *      parameters:
 *      - name: orderId
 *        description: Order id
 *        in: path
 *        required: true
 *        type: integer
 *      - name: productId
 *        description: Product id
 *        in: path
 *        required: true
 *        type: integer
 *      - name: quantity
 *        description: Total amount of each item in the order
 *        in: formData
 *        required: true
 *        type: integer
 *      - name: notes
 *        description: Some additional notes
 *        in: formData
 *        required: false
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/:orderId/products/:productId", function (req, res) {
  let orderId = parseInt(req.params.orderId, 10);
  let productId = parseInt(req.params.productId, 10);
  res.json({
    msj: orders.productUpdate(req.body, orderId, productId),
  });
});

/**
 * @swagger
 * paths:
 *   /orders/{id}/confirms:
 *    post:
 *      tags: [Orders]
 *      description: Updates the status to confirmed
 *      summary: "Updates the status to confirmed "
 *      parameters:
 *      - name: id
 *        description: Id Order to let the status change
 *        in: path
 *        required: true
 *        type: integer
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/:id/confirms", function (req, res, next) {
  let id = parseInt(req.params.id, 10);
  let statusId = parseInt(2, 10);
  res.json({
    msj: orders.statusUpdate(id, statusId),
  });
});

/**
 * @swagger
 * paths:
 *   /orders/{id}/cancells:
 *    post:
 *      tags: [Orders]
 *      description: Updates the status to cancelled
 *      summary: "Updates the status to cancelled "
 *      parameters:
 *      - name: id
 *        description: Id Order to let the status change
 *        in: path
 *        required: true
 *        type: integer
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/:id/cancells", function (req, res) {
  let id = parseInt(req.params.id, 10);
  let statusId = parseInt(6, 10);
  res.json({
    msj: orders.statusUpdate(id, statusId, true),
  });
});

/**
 * @swagger
 * paths:
 *   /orders/{id}:
 *    delete:
 *      tags: [Orders]
 *      description: Deletes an order by id
 *      summary: "Deletes an order by id"
 *      parameters:
 *      - name: id
 *        description: Order id to delete
 *        in: path
 *        type: String
 *      responses:
 *        200:
 *          description: Success
 */
router.delete("/:id", async (req, res) => {
  const { id } = req.params;
  orders.delet({ id }, res);
});
/**
 * @swagger
 * definitions:
 *   Orders:
 *     type: "object"
 *     properties:
 *       id:
 *         type: "integer"
 *         format: "int64"
 *       status:
 *         type: "integer"
 *         format: "int64"
 *         $ref: "#/definitions/Status"
 *       date:
 *         type: "string"
 *         format: "date"
 *       time:
 *         type: "string"
 *         format: "hour"
 *       user:
 *         type: "string"
 *         $ref: "#/definitions/Users"
 *       delivery_address:
 *         type: "string"
 *       payment_opt:
 *         type: "integer"
 *         format: "int64"
 *         $ref: "#/definitions/payment_opt"
 *       to_pay:
 *         type: "integer"
 *         format: "int64"
 *       products:
 *         $ref: "#/definitions/productList"
 *   productList:
 *     type: "object"
 *     properties:
 *       productId:
 *         type: "integer"
 *         format: "int64"
 *         $ref: "#/definitions/Products"
 *       quantity:
 *         type: "integer"
 *         format: "int64"
 *       unitary_price:
 *         type: "integer"
 *         format: "int64"
 *       notes:
 *         type: "string"
 *       date:
 *         type: "string"
 *         format: "date"
 *       time:
 *         type: "string"
 *         format: "hour"
 *   Status:
 *     type: "object"
 *     properties:
 *       id:
 *         type: "integer"
 *         format: "int64"
 *       status:
 *         type: "string"
 *       detail:
 *         type: "string"
 */
//});

/**
 * @swagger
 * paths:
 *   /orders/status/{id}:
 *    delete:
 *      tags: [Status]
 *      description: Deletes a status by id
 *      summary: "Deletes a status by id"
 *      parameters:
 *      - name: id
 *        description: Id to delete
 *        in: path
 *        type: String
 *      responses:
 *        200:
 *          description: Success
 */
router.delete("/status/:id", function (req, res, next) {
  const { id } = req.params;
  orderStatusSchema
    .deleteOne({ _id: id })
    .then((data) =>
      data.deletedCount == 0
        ? res.json(`Status doesn't exists`)
        : res.json(`Status deleted`)
    )
    .catch((error) => res.json({ message: "error" }));
});

module.exports = router;
