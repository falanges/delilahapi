require("dotenv").config();
const cors = require("cors");
const jwt = require("jsonwebtoken");
let activeUsers = [];

const userLoggin = (req, res, next) => {
  activeUsers.length === 0 ? res.json(`Nobody logged yet`) : next();
};

const adminUser = (req, res, next) => {
  const detoken = jwt.verify(activeUsers[0], process.env.JWT_SECRET);
  detoken.role === "admin" ? next() : res.json(`This user cannot do that`);
};

const regularUser = (req, res, next) => {
  const detoken = jwt.verify(activeUsers[0], process.env.JWT_SECRET);

  detoken.role === ("regular" || "admin")
    ? next()
    : res.json(`This user cannot do that`);
};

module.exports = {
  userLoggin,
  adminUser,
  regularUser,
  activeUsers,
};
