const mongoose = require("mongoose");
const userSchema = require("../models/db.userSchema");
//const productSchema = require("../models/db.productSchema");
const orderSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users",
    required: false,
    //ref: "userSchema",
  },
  //status: {
  //  type: String, // me lo va a traer orderStatusSchema
  //  required: false,
  //},
  // date: { type: Date,
  //     default: Date.now,
  // },
  // address: {                      // lo llena el user
  //     type: String,
  //     required: true
  // },
  // payment_opt: {                // lo llena el user
  //     type: String,
  //     required: true
  // },
  // to_pay: {                     //hacer funcion de suma de productos
  //     type: Number,
  //     required: false
  // },
  // products: [{
  //     product: {
  //         type: mongoose.Schema.Types.ObjectId,
  //         required: true,
  //         ref: 'productSchema'
  //     },
  //     quantity: {
  //         type: Number,
  //         required: true
  //     },
  //     unitPrice: {
  //         type: Number,
  //         required: false
  //     },
  //     amount: {
  //         type: Number,
  //         required: false
  //     },
  //     notes: {
  //         type: String,
  //         required: false
  //     }
  // }]
});
module.exports = mongoose.model("orders", orderSchema);
