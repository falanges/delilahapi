const mongoose = require("mongoose");
const userSchema = mongoose.Schema({
    user: {
        type: String,
        required: true,
    },
    fullname: {
        type: String,
        required: true,
    },
    mail: { type: String,
    required: true,
    unique: true,
    lowercase: true,
    },
    phone: { type: String,
    required: true,
    },
    address: { type: [],
    required: true,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: false,
    }
});

module.exports = mongoose.model('users', userSchema);