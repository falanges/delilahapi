const mongoose = require('mongoose');
const paymentSchema = mongoose.Schema({
    type: { 
        type: String, 
        required: true
    }
});

module.exports = mongoose.model('payments', paymentSchema);