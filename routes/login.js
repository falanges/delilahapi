const userSchema = require("../models/db.userSchema");
const jwt = require("jsonwebtoken");
const express = require(`express`);
const router = express.Router();
const cors = require("../middlewares/cors.js");
router.use(
  express.urlencoded({
    extended: true,
  })
);
const users = require("../functions/users");
/**
 * @swagger
 * tags:
 *   name: Login
 *   description: Login behaviour.
 * paths:
 *   /login:
 *    post:
 *      tags: [Login]
 *      description: User login
 *      summary: "Signin"
 *      parameters:
 *      - name: user
 *        description: Username
 *        in: formData
 *        required: true
 *        type: string
 *      - name: password
 *        description: Users password
 *        in: formData
 *        required: true
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/login", async (req, res, next) => {
  let msg = await users.login(req.body.user, req.body.password);
  res.json({
    msg,
  });
});

/**
 * @swagger
 * paths:
 *   /view:
 *    get:
 *      tags: [Login]
 *      description: Displays current user
 *      summary: "Displays current user"
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/view", async (req, res) => {
  let exists;
  try {
    let user = users.get();
    const detoken = await jwt.verify(user[0], process.env.JWT_SECRET);
    exists = await userSchema.findOne({ _id: detoken._id });
  } catch (error) {
    exists = `Yep sure`;
  }
  res.json(exists);
});

/**
 * @swagger
 * paths:
 *   /new:
 *    post:
 *      tags: [Login]
 *      description: Creates a new user
 *      summary: "Signup"
 *      parameters:
 *      - name: user
 *        description: Name of the user.
 *        in: formData
 *        required: true
 *        type: string
 *      - name: fullname
 *        description: Full name of the user.
 *        in: formData
 *        required: true
 *        type: string
 *      - name: mail
 *        description: User email.
 *        in: formData
 *        required: true
 *        type: string
 *      - name: phone
 *        description: User phone number.
 *        in: formData
 *        required: true
 *        type: integer
 *      - name: address
 *        description: User address.
 *        in: formData
 *        required: true
 *        type: string
 *      - name: password
 *        description: User password.
 *        in: formData
 *        required: true
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 *
 */
router.post("/new", async (req, res) => {
  let newUser = await req.body;
  newUser.role = "regular";
  res.json({
    msj: await users.add(newUser),
  });
});

/**
 * @swagger
 * paths:
 *   /logout:
 *    delete:
 *      tags: [Login]
 *      description: Kicks off the current user
 *      summary: "Logout"
 *      responses:
 *        200:
 *          description: Success
 */
router.delete("/logout", function (req, res) {
  res.json(users.logout());
});

module.exports = router;
