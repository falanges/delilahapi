require("dotenv").config();
const redis = require("redis");
const bluebird = require("bluebird");
bluebird.promisifyAll(redis);

const redisClient = redis.createClient({
  host: "redis",
  port: 6379,
});
redisClient.on("error", (err) => {
  console.error(err);
});
console.log("Connected to Redis");
module.exports = { redisClient };
