require("dotenv").config();
const jwt = require("jsonwebtoken");
const userSchema = require("../models/db.userSchema");
const volatileUser = require("../middlewares/users.js");
const orderSchema = require("../models/db.orderSchema");
const paymentSchema = require("../models/db.paymentSchema");
const productSchema = require("../models/db.productSchema");
const orderStatusSchema = require("../models/db.orderStatusSchema");

let get = async () => {
  try {
    let getter = await orderSchema.find();
    return getter;
  } catch (e) {
    console.error(e);
  }
};

let getUserOrders = async () => {
  let getter = await orderSchema.find({ _id: userSchema._id });
  return getter;
};

let getOne = async () => {
  let getter = await orderStatusSchema.findOne({ _id: order.id });
  return getter;
};

let getStatus = async () => {
  let getter = await orderStatusSchema.findOne();
  return getter;
};

let addNewStatus = async (data) => {
  let statusData = await orderStatusSchema.findOne({ _id: data._id });
  if (statusData === null) {
    let givedStatus = orderStatusSchema(data);
    givedStatus.save();
    console.log(givedStatus);
    return `Status gracefully added`;
  } else {
    return `Something has been wrong, please contact support`;
  }
};

let getUser = () => {
  let username = volatileUser.activeUsers.users;
  return (getter = volatileOrder.orders
    .sort((a, b) => a.id - b.id)
    .filter(function (order) {
      return order.users === username;
    }));
};

let orderFinder = async (order) => {
  return (exists = await orderSchema.findOne({ _id: order.id }));
};

const orderData = async (data, add) => {
  console.log(data);
  switch (add) {
    case "addUser":
      let user = await jwt.verify(
        volatileUser.activeUsers[0],
        process.env.JWT_SECRET
      );
      let userData = await userSchema.findOne({ _id: user._id });
      break;
    case "addProduct":
      let product = await productSchema.findOne({ _id: user._id });
      //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor2
      break;
  }
};

let addProdToOrder = async (data, add) => {
  const detoken = await jwt.verify(
    volatileUser.activeUsers[0],
    process.env.JWT_SECRET
  );
  let userData = await userSchema.findOne({ _id: detoken._id });
  let productData = await productSchema.find();
};

let update = (orderUpdate, id) => {
  let exists = orderFinder(id);
  if (exists === -1) {
    return `Can't find user (if logged)!`;
  } else {
    if (volatileOrder.orders[exists].status === 2) {
      return "Order closed. Generate a new one instead.";
    } else {
      orderUpdate.users = volatileUser.activeUsers.users;
      if (orderUpdate.delivery_address === undefined) {
        orderUpdate.delivery_address =
          volatileUser.activeUsers.delivery_address;
      }
      orderUpdate.payment_opt = parseInt(orderUpdate.payment_opt, 10);
      orderUpdate.id = id;
      orderUpdate.status = volatileOrder.orders[exists].status;
      orderUpdate.Orders = volatileOrder.orders[exists].Orders;
      let f = new Date();
      orderUpdate.date = `${f.getDate()}/${f.getMonth()}/${f.getFullYear()}`;
      orderUpdate.time = `${f.getHours()}:${f.getMinutes()}`;
      volatileOrder.orders[exists] = orderUpdate;
      return `Order updated`;
    }
  }
};

let statusUpdate = (id, statusId, cancells) => {
  let exists = orderFinder(id);
  if (exists === -1) {
    return `Can't find user (if logged)!`;
  } else {
    if (cancells === true) {
      if (volatileOrder.orders[exists].status === 5) {
        return `Order already delivered. You can't cancell it. Say Sorry.`;
      }
    }
    volatileOrder.orders[exists].status = statusId;
    let f = new Date();
    volatileOrder.orders[
      exists
    ].date = `${f.getDate()}/${f.getMonth()}/${f.getFullYear()}`;
    volatileOrder.orders[exists].time = `${f.getHours()}:${f.getMinutes()}`;
    return `Order updated successfully`;
  }
};

let delet = async (id, res) => {
  let exists = await orderFinder(id);
  if (exists === null) {
    res.json(`Order doesn't exists. Please check it out`);
  } else {
    await orderSchema
      .deleteOne({ id })
      .then((data) => res.json("Order deleted sweethearth"))
      .catch((err) => res.json({ message: `Something gone bad darling` }));
  }
};

let updateOrderPrice = (orderId) => {
  let orderPrice = 0;
  volatileOrder.orders[orderId].Orders.forEach((Order) => {
    let subtotal = Order.unitary_price * Order.quantity;
    orderPrice += subtotal;
  });
  volatileOrder.orders[orderId].to_pay = orderPrice;
};

let addOrder = async (newOrder) => {
  newOrder.status = orderStatusSchema.findOne(`61a29bdad46f547f5fd88735`);
  let order = orderSchema(newOrder);
  order.save();
  return (msg = {
    text: `Order created. Your are one step closer to eat like a Viking`,
  });
};

let orderUpdate = (ordUpdate, orderId, OrderId) => {
  let f = new Date();
  ordUpdate.date = `${f.getDate()}/${f.getMonth()}/${f.getFullYear()}`;
  ordUpdate.time = `${f.getHours()}:${f.getMinutes()}`;
  ordUpdate.quantity = parseInt(ordUpdate.quantity, 10);
  let exists = orderFinder(orderId);
  if (exists === -1) {
    return "Can't find user (if logged)!";
  } else {
    if (volatileOrder.orders[exists].status === 2) {
      return "Can't modify: Order already setted up.";
    } else {
      let OrderExists = volatileOrder.orders[exists].Orders.findIndex(
        ({ OrderId }) => OrderId === OrderId
      );
      if (OrderExists === -1) {
        return "Not exists on your order";
      } else {
        ordUpdate.OrderId = OrderId;
        ordUpdate.unitary_price =
          volatileOrder.orders[exists].Orders[OrderExists].unitary_price;
        volatileOrder.orders[exists].Orders[OrderExists] = ordUpdate;
        updateOrderPrice(exists);
        return `Updated correctly`;
      }
    }
  }
};

module.exports = {
  get,
  getOne,
  getUser,
  getStatus,
  getUserOrders,
  addProdToOrder,
  addNewStatus,
  addOrder,
  update,
  statusUpdate,
  orderUpdate,
  orderData,
  delet,
};
