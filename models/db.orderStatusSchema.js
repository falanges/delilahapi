const mongoose = require('mongoose');
const statusSchema = mongoose.Schema({
    status: {
        type: String,
        default: 'To Do',
        required: true
    },
})

module.exports = mongoose.model('orderStatus', statusSchema);