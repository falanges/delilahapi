require("dotenv").config();
const productSchema = require("../models/db.productSchema");
const productCache = require("../db.redisconf");
const redis = require("redis");
const bluebird = require("bluebird");
bluebird.promisifyAll(redis);
const client = redis.createClient({
  host: "redis",
  port: 6379,
});
client.on("error", function (error) {
  console.error(error);
});
client.set("products", "", "EX", 1); // >>> esto es para borrar cache de productos

const get = async (req, res) => {
  let aux = await client.getAsync("products");
  if (aux == null) {
    let products = await productSchema.find();
    client.set("products", JSON.stringify(products), "EX", 3600);
    //console.log("database");
    res.json(products);
  } else {
    res.json(JSON.parse(aux));
    //console.log("cache");
  }
};

let productFinder = async (product) => {
  return (exists = await productSchema.findOne({ _id: product.id }));
};

let add = async (newProduct) => {
  let exists = await productSchema.findOne({
    description: newProduct.description,
  });
  if (exists === null) {
    let someProduct = await productSchema(newProduct);
    await someProduct.save();
    let products = await productSchema.find();
    client.set("products", JSON.stringify(products), "EX", 3600);
    return `Product added`;
  } else {
    console.log("re que existe");
    return `Product already exists`;
  }
};

let update = async (productUpdate, res) => {
  let exists = await productFinder(productUpdate);
  if (exists === null) {
    return `There is no product who matches your specifications`;
  } else {
    if (productUpdate.description !== undefined) {
      exists.description = productUpdate.description;
    }
    if (productUpdate.price !== undefined) {
      exists.price = productUpdate.price;
    }
    await productSchema
      .updateOne(
        { _id: exists.id },
        {
          $set: {
            description: exists.description,
            price: exists.price,
          },
        }
      )
      .then((data) => res.json("Product modified successfully"))
      .catch((error) => res.json({ message: "Something has been wrong" }));
    let products = await productSchema.find();
    client.set("products", JSON.stringify(products), "EX", 3600);
  }
};

let delet = async (id, res) => {
  let exists = await productFinder(id);
  if (exists === null) {
    res.json(`Product doesnt exists`);
  } else {
    await productSchema
      .deleteOne({ _id: exists.id })
      .then((data) => res.json("Product deleted"))
      .catch((error) => res.json({ message: "Something has been wrong" }));
    let products = await productSchema.find();
    client.set("products", JSON.stringify(products), "EX", 3600);
  }
};

module.exports = {
  get,
  add,
  delet,
  update,
  productCache,
  productFinder,
};
