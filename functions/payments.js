const paymentSchema = require("../models/db.paymentSchema");

let get = async () => {
  let gg = await paymentSchema.find({});
  return gg;
};

let payment_method_finder = async (payment) => {
  return (exists = await paymentSchema.findOne({ _id: payment.id }));
};

let add = async (newPayment) => {
  let exists = await paymentSchema.findOne({ type: newPayment.type });
  if (exists === null) {
    let goodPayment = paymentSchema(newPayment);
    goodPayment.save();
    return `New Payment added`;
  } else {
    return `Something gone bad`;
  }
};

let update = async (payment_update, res) => {
  let exists = await payment_method_finder(payment_update);
  if (exists === null) {
    return `This payment method doesn't exist.`;
  } else {
    if (payment_update.type !== undefined) {
      exists.type = payment_update.type;
    }
    paymentSchema
      .updateOne(
        { _id: exists.id },
        {
          $set: {
            type: exists.type,
          },
        }
      )
      .then((data) => res.json("Payment modified successfully"))
      .catch((error) => res.json({ message: "Something has been wrong" }));
  }
};

let delet = (id) => {
  let index = payment_method_finder(id);
  if (index === -1) {
    return `The payment method ${id} doesn't exist`;
  } else {
    volatile_payment_methods.payments.splice(index, 1);
    return `Payment method ${id} deleted successfully`;
  }
};

module.exports = {
  get,
  add,
  payment_method_finder,
  update,
  delet,
};
