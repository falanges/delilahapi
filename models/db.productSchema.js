const mongoose = require('mongoose');
const productSchema = mongoose.Schema({
    description: {
        type: String,
        required: true
    },
    price: { 
        type: Number,
        required: true
    }
})

module.exports = mongoose.model('products', productSchema);