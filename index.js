require("dotenv").config();
const fetch = require("cross-fetch");
const express = require("express");
const cors = require("cors");
const app = express();
const port = process.env.PORT || 4000;
app.use(express.json());
const midUsers = require("./middlewares/users");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
const connectionString = process.env.MONGO_CONN_STR;
const Sentry = require("@sentry/node");
const SentryTracing = require("@sentry/tracing");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "Delilah Resto API",
      version: "1.0.0",
      description:
        " A simple way to get your food orders with no mistakes or delays between the order and yourself",
      contact: {
        name: "Matias Parache",
        url: "http://localhost:4000/",
        email: "matiasparache@pm.me",
      },
    },
  },
  apis: [
    "./index.js",
    "./routes/login.js",
    "./routes/orders.js",
    "./routes/users.js",
    "./routes/products.js",
    "./routes/payments.js",
  ],
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocs));
const helmet = require("helmet");
app.use(helmet());
const login = require("./routes/login");
app.use(cors());
app.use("/", login);
const users = require("./routes/users");
app.use("/users", [midUsers.userLoggin, midUsers.adminUser], users);
var products = require("./routes/products");
app.use("/products", [midUsers.userLoggin, midUsers.adminUser], products);
var orders = require("./routes/orders");
app.use("/orders", [midUsers.userLoggin, midUsers.adminUser], orders);
var payments = require("./routes/payments");
app.use("/payments", [midUsers.userLoggin, midUsers.adminUser], payments);

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});

app.use(helmet());
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
async function main() {
  await mongoose
    .connect(connectionString, options)
    .then(() => console.log("Connected to MongoDB Atlas"))
    .catch((error) => console.error(error));
}
main().catch((err) => console.log(err));

Sentry.init({
  dsn: "https://da533c56cca64221b3a40302c1515a76@o1181929.ingest.sentry.io/6295684",
  integrations: [
    // enable HTTP calls tracing
    new Sentry.Integrations.Http({ tracing: true }),
    // enable Express.js middleware tracing
    new SentryTracing.Integrations.Express({ app }),
  ],
  tracesSampleRate: 1.0,
});

const transaction = Sentry.startTransaction({
  op: "test",
  name: "My First Test Transaction",
});

setTimeout(() => {
  try {
    foo();
  } catch (e) {
    Sentry.captureException(e);
  } finally {
    transaction.finish();
  }
}, 99);

app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());

app.use(Sentry.Handlers.errorHandler());

module.exports = mongoose;
