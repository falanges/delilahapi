const express = require("express");
const router = express.Router();
const adminUser = require("../middlewares/users");
router.use(express.urlencoded({
    extended: true
}));
const listProduct = require('../functions/products')
const productSchema = require("../models/db.productSchema");

/**
 * @swagger
 * tags:
 *   name: Products
 *   description: CRUD product operations (Only admins).
 * paths:
 *   /products:
 *    get:
 *      tags: [Products]
 *      description: Get all products
 *      summary: "Get all products"
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/", async (req, res ) => {
    listProduct.get(req, res);
});

/**
 * @swagger
 * paths:
 *   /products:
 *    post:
 *      tags: [Products]
 *      description: Creates new product
 *      summary: "Creates new product"
 *      parameters:
 *      - name: description
 *        description: Product data
 *        in: formData
 *        required: true
 *        type: string
 *      - name: price
 *        description: Product price
 *        in: formData
 *        required: true
 *        type: integer
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/", adminUser.adminUser, async (req, res, next) => {
    const product = await listProduct.add(req.body);
    res.json(product);
});

/**
 * @swagger
 * paths:
 *   /products/{id}:
 *    put:
 *      tags: [Products]
 *      description: Product update
 *      summary: "Product update"
 *      parameters:
 *      - name: id
 *        description: Target id of product to update
 *        in: path
 *        required: true
 *        type: string
 *      - name: description
 *        description: Leave blank if you don't deserve to modify
 *        in: formData
 *        required: false
 *        type: string
 *      - name: price
 *        description: Price. Leave blank if you don't deserve to modify
 *        in: formData
 *        required: true
 *        type: integer
 *      responses:
 *        200:
 *          description: Success
 */
router.put('/:id', adminUser.adminUser, async (req, res, next) => {
  const { id } = req.params;
  const { description, price } = req.body;
  listProduct.update({id, description, price}, res);
});

/**
 * @swagger
 * paths:
 *   /products/{id}:
 *    delete:
 *      tags: [Products]
 *      description: Deletes product by id
 *      summary: "Deletes product by id"
 *      parameters:
 *      - name: id
 *        description: Product id to exterminate
 *        in: path
 *        type: string
 *        required: true
 *      responses:
 *        200:
 *          description: Success
 */
router.delete("/:id", adminUser.adminUser, (req, res, next) => {
    const { id } = req.params;
    listProduct.delet({id}, res);
});

/**
 * @swagger
 * definitions:
 *   Products:
 *     type: "object"
 *     properties:
 *       id:
 *         type: "integer"
 *         format: "int64"
 *       description:
 *         type: "string"
 *       price:
 *         type: "integer"
 *         format: "int64"
 */

module.exports = router;