const express = require('express');
const userSchema = require("../models/db.userSchema");
const router = express.Router();
router.use(express.urlencoded({
    extended: true
}));
const users = require('../functions/users');

/**
 * @swagger
 * paths:
 *   /users:
 *    post:
 *      tags: [User]
 *      description: Creates new user.
 *      summary: "Creates a new user"
 *      parameters:
 *      - name: user
 *        description: The name of the user.
 *        in: formData
 *        required: true
 *        type: string
 *      - name: fullname
 *        description: The full name of the user.
 *        in: formData
 *        required: true
 *        type: string
 *      - name: mail
 *        description: The users email.
 *        in: formData
 *        required: true
 *        type: string
 *      - name: phone
 *        description: The telephone of the user.
 *        in: formData
 *        required: true
 *        type: integer
 *      - name: address
 *        description: The address of the user.
 *        in: formData
 *        required: true
 *        type: string
 *      - name: password
 *        description: The password of the user.
 *        in: formData
 *        required: true
 *        type: string
 *      - name: role
 *        description: The role who determines his own capabilities, admin or regular user
 *        in: formData
 *        required: true
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 *
 */
router.post('/', async (req, res) => {
    const user = await users.add(req.body);
    res.json(user);

});

/**
 * @swagger
 * tags:
 *   name: User
 *   description: CRUD powers. 
 * paths:
 *   /users:
 *    get:
 *      tags: [User]
 *      description: Gets all userlist
 *      summary: "Gets all userlist"
 *      responses:
 *        200:
 *          description: Success
 */
router.get('/', async (req, res, next) => {
    let allUsers = await userSchema.find();
    res.json(allUsers); 
});

/**
 * @swagger
 * paths:
 *   /users/{id}:
 *    put:
 *      tags: [User]
 *      description: User update
 *      summary: "User update"
 *      parameters:
 *      - name: id
 *        description: Find user by Name
 *        in: path
 *        required: true
 *        type: string
 *      - name: user
 *        description: New username (Leave this field blank if you don't need to change)
 *        in: formData
 *        required: false
 *        type: string
 *      - name: fullname
 *        description: Full name of the user
 *        in: formData
 *        required: false
 *        type: string
 *      - name: mail
 *        description: Users email
 *        in: formData
 *        required: false
 *        type: string
 *      - name: phone
 *        description: Users phone number
 *        in: formData
 *        required: false
 *        type: integer
 *      - name: address
 *        description: Destination address of the orders
 *        in: formData
 *        required: false
 *        type: string
 *      - name: password
 *        description: Users password
 *        in: formData
 *        required: false
 *        type: string
 *      - name: role
 *        description: Users role - select admin or regular
 *        in: formData
 *        required: false
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const { user, fullname, mail, phone, address, password } = req.body;
    users.update({id, user, fullname, mail, phone, address, password}, res);
});
    
/**
 * @swagger
 * paths:
 *   /users/{id}:
 *    delete:
 *      tags: [User]
 *      description: Erradicates the user forever
 *      summary: "User deletion"
 *      parameters:
 *      - name: id
 *        description: Id to delete from
 *        in: path
 *        type: String
 *      responses:
 *        200:
 *          description: Success
 */
router.delete('/:id', function(req, res, next) {
    const { id } = req.params;
    userSchema
      .deleteOne({ _id: id })
      .then((data) => (data.deletedCount == 0) 
      ? (res.json(`User doesn't exists`)) 
      : (res.json(`User deleted`)))
      .catch((error) => res.json({ message: 'You are inmortal >=D' }));
});

module.exports = router;