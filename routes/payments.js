const express = require("express");
const router = express.Router();
const paymentSchema = require("../models/db.paymentSchema");
router.use(
  express.urlencoded({
    extended: true,
  })
);
const payments = require("../functions/payments.js");

/**
 * @swagger
 * tags:
 *   name: Payment_methods
 *   description: Payment operations for admin users.
 * paths:
 *   /payments:
 *    get:
 *      tags: [Payment_methods]
 *      summary: "Gives us all payment possibilities"
 *      description: Gives us all payment possibilities
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/", async (req, res) => {
  res.json(await payments.get());
});

/**
 * @swagger
 * paths:
 *   /payments:
 *    post:
 *      tags: [Payment_methods]
 *      description: Creates a new payment method
 *      summary: "Creates a new payment method"
 *      parameters:
 *      - name: type
 *        description: New method type
 *        in: formData
 *        required: true
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/", async (req, res) => {
  const payment = await payments.add(req.body);
  res.json(payment);
});

/**
 * @swagger
 * paths:
 *   /payments/{id}:
 *    put:
 *      tags: [Payment_methods]
 *      description: Updates an existing payment method
 *      summary: "Updates an existing payment method"
 *      parameters:
 *      - name: id
 *        description: id method to change
 *        in: path
 *        required: true
 *        type: string
 *      - name: type
 *        description: Modifies existing method
 *        in: formData
 *        required: true
 *        type: string
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const { type } = req.body;
  payments.update({ id, type }, res);
});

/**
 * @swagger
 * paths:
 *   /payments/{id}:
 *    delete:
 *      tags: [Payment_methods]
 *      description: Erases a payment method
 *      summary: "Erases a payment method by id"
 *      parameters:
 *      - name: id
 *        description: Id to make deletion
 *        in: path
 *        type: String
 *      responses:
 *        200:
 *          description: Success
 */
router.delete("/:id", function (req, res) {
  const { id } = req.params;
  paymentSchema
    .deleteOne({ _id: id })
    .then((data) =>
      data.deletedCount == 0
        ? res.json(`Payment doesn't exists'`)
        : res.json(`Payment deleted`)
    )
    .catch((error) => res.json({ message: "Algo" }));
});

/**
 * @swagger
 * definitions:
 *   payments:
 *     type: "object"
 *     properties:
 *       id:
 *         type: "integer"
 *         format: "int64"
 *       type:
 *         type: "string"
 */

module.exports = router;
