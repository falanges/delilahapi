## Pre-requisites:

    Docker, docker-compose and npm are required. You can follow the installation instructions at https://docs.docker.com/get-started/#download-and-install-docker for docker, https://docs.docker.com/compose/install/ for docker-compose install, and https://docs.npmjs.com/downloading-and-installing-node-js-and-npm.

Please install this project by making the following steps:

- 1. Clone this repository & cd them:

```$ git clone https://gitlab.com/falanges/delilahapi.git && cd delilahapi```

- 2. Run node package manager installer and then init the docker compose file:

```npm i && docker-compose up -d```

- 3. On your browser, enter at url <<http://localhost:4000/api-docs>>

That is it.

##Note: 
I have mounted the same proyect over the following domain, if you are lazy and don't want to install on your own pc:

https://www.delilahdelivery.tk/api-docs

Send me your thoughts and feedback at matiasparache@pm.me . The only thing that makes me grow is to open my eyes in front of my own faults and mistakes.